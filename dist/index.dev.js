"use strict";

var navUl = document.querySelector("#navUl");
var hamburgerIcon = document.querySelector("#hamburgerIcon");
var nav = document.querySelector("nav");
hamburgerIcon.addEventListener("click", hamburgerToggle);

function hamburgerToggle() {
  nav.classList.toggle("h-full");
  nav.classList.toggle("hidden");
  navUl.classList.toggle("bg-gray-900");
  navUl.classList.toggle("bg-opacity-70");
  navUl.classList.toggle("h-screen");
  hamburgerIcon.classList.toggle("mNav-open");
  hamburgerIcon.children[0].classList.toggle("rotate-45");
  hamburgerIcon.children[0].classList.toggle("border-white");
  hamburgerIcon.children[1].classList.toggle("hidden");
  hamburgerIcon.children[2].classList.toggle("w-6");
  hamburgerIcon.children[2].classList.toggle("-rotate-45");
  hamburgerIcon.children[2].classList.toggle("border-white");
}

document.addEventListener("scroll", function () {
  if (document.documentElement.getBoundingClientRect().top < -100) {
    nav.classList.add("md:shadow-xl", "md:bg-gray-200");
  } else {
    nav.classList.remove("md:shadow-xl", "md:bg-gray-200");
  }
});
navUl.childNodes.forEach(function (child) {
  child.addEventListener("click", function () {
    if (hamburgerIcon.classList.contains("mNav-open")) {
      hamburgerToggle();
    }
  });
}); // smooth scroll
// Smooth scroll

$(document).ready(function () {
  // Add smooth scrolling to all links
  $("a").on("click", function (event) {
    // Make sure this.hash has a value before overriding default behavior
    if (this.hash !== "") {
      // Prevent default anchor click behavior
      event.preventDefault(); // Store hash

      var hash = this.hash; // Using jQuery's animate() method to add smooth page scroll
      // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area

      $("html, body").animate({
        scrollTop: $(hash).offset().top
      }, 800, function () {
        // Add hash (#) to URL when done scrolling (default click behavior)
        window.location.hash = hash;
      });
    } // End if

  });
}); // form submit
// mail

var form = document.getElementById("form");
var result = document.getElementById("result");
form.addEventListener("submit", function (e) {
  var formData = new FormData(form);
  e.preventDefault();
  var object = {};
  formData.forEach(function (value, key) {
    object[key] = value;
  });
  var json = JSON.stringify(object);
  result.innerHTML = "Please wait...";
  fetch("https://api.web3forms.com/submit", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      Accept: "application/json"
    },
    body: json
  }).then(function _callee(response) {
    var json;
    return regeneratorRuntime.async(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return regeneratorRuntime.awrap(response.json());

          case 2:
            json = _context.sent;

            if (response.status == 200) {
              result.innerHTML = json.message;
            } else {
              console.log(response);
              result.innerHTML = json.message;
            }

          case 4:
          case "end":
            return _context.stop();
        }
      }
    });
  })["catch"](function (error) {
    console.log(error);
    result.innerHTML = "Something went wrong!";
  }).then(function () {
    form.reset();
    setTimeout(function () {
      result.style.display = "none";
    }, 3000);
  });
});